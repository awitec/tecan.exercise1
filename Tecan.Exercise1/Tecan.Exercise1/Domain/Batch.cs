﻿using System.Collections.Generic;

namespace Tecan.Exercise1.Domain
{
    public class Batch
    {
        private readonly IList<Job> _jobs = new List<Job>();

        public Batch(string id)
        {
            Id = id;
        }

        public Batch AddJob(Job job)
        {
            _jobs.Add(job);

            return this;
        }

        public string Id { get; }

        public IEnumerable<Job> Jobs => _jobs;
    }
}
