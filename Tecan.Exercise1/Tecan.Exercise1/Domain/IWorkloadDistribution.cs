﻿using System.Collections.Generic;

namespace Tecan.Exercise1.Domain
{
    public interface IWorkloadDistribution
    {
        IEnumerable<Operation> this[string machineId] { get; }

        IEnumerable<string> MachineIds { get; }
    }
}