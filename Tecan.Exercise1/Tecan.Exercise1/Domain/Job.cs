﻿using System.Collections.Generic;

namespace Tecan.Exercise1.Domain
{
    public class Job
    {
        private IList<Operation> _operations = new List<Operation>();

        public Job(string id)
        {
            Id = id;
        }

        public Job AddOperation(Operation operation)
        {
            _operations.Add(operation);

            return this;
        }

        public string Id { get; }

        public IEnumerable<Operation> Operations => _operations;
    }
}
