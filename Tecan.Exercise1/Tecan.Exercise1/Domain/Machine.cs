﻿using System;

namespace Tecan.Exercise1.Domain
{
    public class Machine
    {
        public Machine(string id)
        {
            Id = id;
        }

        public void ProcessOperation(Operation operation)
        {
            if (operation.MachineId.Equals(Id))
            {
                throw new InvalidOperationException();
            }

            Console.WriteLine($"MachineId {Id} has completed operation: {operation.DoStuff()}");
        }

        public string Id { get; }
    }
}
