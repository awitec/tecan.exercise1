﻿namespace Tecan.Exercise1.Domain
{
    public abstract class Operation
    {
        public Operation(string id, string jobId, string machineId)
            : this(id, jobId, machineId, null) { }

        public Operation(string id, string jobId, string machineId, uint? order)
        {
            Id = id;
            JobId = jobId;
            MachineId = machineId;
            Order = order;
        }

        public string Id { get; }

        public string JobId { get; }

        public string MachineId { get; }

        public uint? Order { get; }

        public abstract uint ExpectedExecutionTimeSeconds { get; }

        public OperationStatus Status { get; set; } = OperationStatus.Queued;

        public abstract string DoStuff();

        public override string ToString()
        {
            return $"OperationId: {Id}, MachineId: {MachineId}, JobId: {JobId}, Duration: {ExpectedExecutionTimeSeconds}s, Order: {Order}";
        }
    }
}
