﻿namespace Tecan.Exercise1.Domain
{
    public enum OperationStatus
    {
        Queued,
        Processing,
        Completed
    }
}
