﻿using System;
using System.Collections.Generic;

namespace Tecan.Exercise1.Domain
{
    public class Workbench
    {
        private readonly IList<Machine> _machines = new List<Machine>();

        public Workbench AddMachine(Machine machine)
        {
            _machines.Add(machine);

            return this;
        }

        public IEquatable<Machine> Machines;
    }
}
