﻿using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Tecan.Exercise1.Domain
{
    public class WorkloadDistribution : IWorkloadDistribution
    {
        private readonly IDictionary<string, List<Operation>> _workloadDistribution = new Dictionary<string, List<Operation>>();

        public WorkloadDistribution AddOperation(Operation operation)
        {
            if (!_workloadDistribution.ContainsKey(operation.MachineId))
            {
                _workloadDistribution.Add(operation.MachineId, new List<Operation>());
            }

            _workloadDistribution[operation.MachineId].Add(operation);

            return this;
        }

        public IEnumerable<string> MachineIds => _workloadDistribution.Keys;

        public IEnumerable<Operation> this[string machineId]
        {
            get
            {
                return _workloadDistribution[machineId];
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            foreach (var machineId in MachineIds)
            {
                sb.AppendLine($"Machine {machineId}: ");

                foreach (var operation in this[machineId])
                {
                    sb.AppendLine($"-- Operation: {operation.Id}, job: {operation.JobId}, duration: {operation.ExpectedExecutionTimeSeconds}s, order: {operation.Order}");
                }
            }

            return sb.ToString();
        }
    }
}
