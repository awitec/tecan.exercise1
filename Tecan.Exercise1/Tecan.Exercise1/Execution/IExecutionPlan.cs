﻿using Tecan.Exercise1.Domain;

namespace Tecan.Exercise1.Execution
{
    public interface IExecutionPlan
    {
        IWorkloadDistribution CalculateWorkloadDistribution(Batch batch);
    }
}
