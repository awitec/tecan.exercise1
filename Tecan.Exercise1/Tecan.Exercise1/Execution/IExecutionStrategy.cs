﻿using Tecan.Exercise1.Domain;

namespace Tecan.Exercise1.Execution
{
    public interface IExecutionStrategy
    {
        Operation GetNextOperation(string machineId);
    }
}
