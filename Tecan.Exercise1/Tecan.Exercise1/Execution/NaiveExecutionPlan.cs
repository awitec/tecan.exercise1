﻿using System.Linq;
using Tecan.Exercise1.Domain;

namespace Tecan.Exercise1.Execution
{
    public class NaiveExecutionPlan : IExecutionPlan
    {
        public IWorkloadDistribution CalculateWorkloadDistribution(Batch batch)
        {
            var workload = new WorkloadDistribution();

            foreach (var job in batch.Jobs)
            {
                foreach (var operation in job.Operations.Where(o => o.Status == OperationStatus.Queued))
                {
                    workload.AddOperation(operation);
                }
            }

            return workload;
        }
    }
}