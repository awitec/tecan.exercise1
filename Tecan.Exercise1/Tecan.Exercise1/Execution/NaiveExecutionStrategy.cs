﻿using System.Linq;
using Tecan.Exercise1.Domain;

namespace Tecan.Exercise1.Execution
{
    public class NaiveExecutionStrategy : IExecutionStrategy
    {
        private readonly IExecutionPlan _executionPlan;
        private readonly Batch _batch;

        public NaiveExecutionStrategy(IExecutionPlan executionPlan, Batch batch)
        {
            _executionPlan = executionPlan;
            _batch = batch;
        }

        public Operation GetNextOperation(string machineId)
        {
            var workload = _executionPlan.CalculateWorkloadDistribution(_batch);

            // check if the previous dependend operation within the job has been finished (might be on the other machine)

            return workload[machineId].First();
        }
    }
}
