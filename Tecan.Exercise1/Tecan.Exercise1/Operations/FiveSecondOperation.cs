﻿using System;
using System.Threading;
using Tecan.Exercise1.Domain;

namespace Tecan.Exercise1.Operations
{
    public class FiveSecondOperation : Operation
    {
        private readonly Random _random = new Random();

        public FiveSecondOperation(string id, string jobId, string machineId)
            : base(id, jobId, machineId) { }

        public FiveSecondOperation(string id, string jobId, string machineId, uint order)
            : base(id, jobId, machineId, order) { }

        public override string DoStuff()
        {
            Status = OperationStatus.Processing;

            var randomSeconds = _random.Next(1, 5);

            Thread.Sleep(((int) ExpectedExecutionTimeSeconds + randomSeconds) * 1000);

            Status = OperationStatus.Completed;

            return ToString();
        }

        public override uint ExpectedExecutionTimeSeconds => 5;
    }
}
