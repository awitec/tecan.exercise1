﻿using System.Threading;
using Tecan.Exercise1.Domain;

namespace Tecan.Exercise1.Operations
{
    public class OneSecondOperation : Operation
    {
        public OneSecondOperation(string id, string jobId, string machineId)
            : base(id, jobId, machineId) { }

        public OneSecondOperation(string id, string jobId, string machineId, uint order)
            : base(id, jobId, machineId, order) { }

        public override string DoStuff()
        {
            Status = OperationStatus.Processing;

            Thread.Sleep((int) ExpectedExecutionTimeSeconds * 1000);

            Status = OperationStatus.Completed;

            return ToString();
        }

        public override uint ExpectedExecutionTimeSeconds => 1;
    }
}
