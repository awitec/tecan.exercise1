﻿using System;
using Tecan.Exercise1.Domain;
using Tecan.Exercise1.Execution;
using Tecan.Exercise1.Operations;

namespace Tecan.Exercise1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Lab Simulator");
            Console.WriteLine();

            var workbench = new Workbench()
                .AddMachine(new Machine("m1"))
                .AddMachine(new Machine("m2"))
                .AddMachine(new Machine("m3"));

            var job1 = new Job("j1")
                .AddOperation(new OneSecondOperation("o1", "j1", "m1", 1))
                .AddOperation(new TwoSecondOperation("o2", "j1", "m2", 2));

            var job2 = new Job("j2")
                .AddOperation(new OneSecondOperation("o3", "j2", "m3", 1))
                .AddOperation(new TwoSecondOperation("o4", "j2", "m2", 2))
                .AddOperation(new FiveSecondOperation("o5", "j2", "m1", 3));

            var job3 = new Job("j3")
                .AddOperation(new OneSecondOperation("o6", "j3", "m3"))
                .AddOperation(new FiveSecondOperation("o7", "j3", "m3"));

            var batch = new Batch("b1")
                .AddJob(job1)
                .AddJob(job2)
                .AddJob(job3);

            var workloadDistribution = new NaiveExecutionPlan().CalculateWorkloadDistribution(batch);

            Console.WriteLine("Workload distribution: " + Environment.NewLine + workloadDistribution);

            Console.ReadLine();
        }
    }
}
